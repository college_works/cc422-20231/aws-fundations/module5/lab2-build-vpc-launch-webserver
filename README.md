# Lab 2: Build Your VPC and Launch a Web Server

**Module:** Module 5 - Networking and Content Delivery (AWS Cloud Foundations)

In this lab we will build the following infrastructure:

![Lab2 infrastructure](./images/lab2_final_product.png)