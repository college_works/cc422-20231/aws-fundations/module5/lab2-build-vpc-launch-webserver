## VPC

resource "aws_vpc" "lab_vpc" {
  cidr_block            = "10.0.0.0/16"
  enable_dns_support    = true
  enable_dns_hostnames  = true
  tags = {
    "Name" = "lab-vpc"
  }
}


## Subnets
resource "aws_subnet" "lab_subnet_public1" {
  vpc_id      = aws_vpc.lab_vpc.id
  cidr_block  = cidrsubnet(aws_vpc.lab_vpc.cidr_block, 8, 0)
  availability_zone = "us-east-1a"

  tags = {
    "Name" = "lab-subnet-public1"
  }
}

resource "aws_subnet" "lab_subnet_private1" {
  vpc_id      = aws_vpc.lab_vpc.id
  cidr_block  = cidrsubnet(aws_vpc.lab_vpc.cidr_block, 8, 1)
  availability_zone = "us-east-1a"
  map_public_ip_on_launch = false

  tags = {
    "Name" = "lab-subnet-private1"
  }
}


resource "aws_subnet" "lab_subnet_public2" {
  vpc_id      = aws_vpc.lab_vpc.id
  cidr_block  = cidrsubnet(aws_vpc.lab_vpc.cidr_block, 8, 2)
  availability_zone = "us-east-1b"

  tags = {
    "Name" = "lab-subnet-public2"
  }
}

resource "aws_subnet" "lab_subnet_private2" {
  vpc_id      = aws_vpc.lab_vpc.id
  cidr_block  = cidrsubnet(aws_vpc.lab_vpc.cidr_block, 8, 3)
  availability_zone = "us-east-1b"

  tags = {
    "Name" = "lab-subnet-private2"
  }
}

# Network connection resources

## Route tables

resource "aws_route_table" "lab_rtb_public" {
  vpc_id = aws_vpc.lab_vpc.id

  tags = {
    Name        = "lab-rtb-public"
  }
}

resource "aws_route_table" "lab_rtb_private" {
  vpc_id = aws_vpc.lab_vpc.id

  tags = {
    Name        = "lab-rtb-private"
  }
}

## Routes

resource "aws_route" "public_internet_gateway" {
  route_table_id         = aws_route_table.lab_rtb_public.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.lab_igw.id
}

resource "aws_route" "private_nat_gateway" {
  route_table_id         = aws_route_table.lab_rtb_private.id
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = aws_nat_gateway.lab_nat_public1.id
}

## Route table association - rta

resource "aws_route_table_association" "lab_rta_public1" {
  subnet_id      = aws_subnet.lab_subnet_public1.id
  route_table_id = aws_route_table.lab_rtb_public.id
}

resource "aws_route_table_association" "lab_rta_public2" {
  subnet_id      = aws_subnet.lab_subnet_public2.id
  route_table_id = aws_route_table.lab_rtb_public.id
}

resource "aws_route_table_association" "lab_rta_private1" { 
  subnet_id      = aws_subnet.lab_subnet_private1.id
  route_table_id = aws_route_table.lab_rtb_private.id
}

resource "aws_route_table_association" "lab_rta_private2" { 
  subnet_id      = aws_subnet.lab_subnet_private2.id
  route_table_id = aws_route_table.lab_rtb_private.id
}

## Internet gateway
resource "aws_internet_gateway" "lab_igw" {
  vpc_id = aws_vpc.lab_vpc.id

  tags = {
    Name = "lab-igw"
  }
}

## NAT gateway
resource "aws_eip" "nat_gw_eip" {
  vpc = true
}

resource "aws_nat_gateway" "lab_nat_public1" {
  allocation_id = aws_eip.nat_gw_eip.id
  subnet_id     = aws_subnet.lab_subnet_public1.id
  depends_on    = [aws_internet_gateway.lab_igw]

  tags = {
    "Name" = "lab-nat-public1"
  }
}