resource "aws_instance" "webserver1" {
  ami           = "ami-06e46074ae430fba6" # us-west-1 - Amazon Linux 2023 AMI
  instance_type = "t2.micro"
	subnet_id =  aws_subnet.lab_subnet_public2.id
	key_name = "vockey"

  vpc_security_group_ids = [aws_security_group.allow_http.id, 
                              aws_security_group.allow_ssh.id]
  user_data = <<-EOF
    #!/bin/bash

    # Install Apache Web Server and PHP
    sudo dnf install -y httpd wget php mariadb105-server

    # Download Lab files
    wget https://aws-tc-largeobjects.s3.us-west-2.amazonaws.com/CUR-TF-100-ACCLFO-2-9026/2-lab2-vpc/s3/lab-app.zip
    unzip lab-app.zip -d /var/www/html/

    # Turn on web server
    chkconfig httpd on
    service httpd start
  EOF


  tags = {
    "Name" = "Web Server 1"
  }
}

resource "aws_eip" "webserver_eip" {
  vpc = true
}

resource "aws_eip_association" "webserver_eip_association" {
  instance_id   = aws_instance.webserver1.id
  allocation_id = aws_eip.webserver_eip.id
}


resource "aws_security_group" "allow_http" {
  name        = "Web Security Group"
  description = "Enable HTTP access"
  vpc_id      = aws_vpc.lab_vpc.id

  ingress {
    description      = "HTTP from VPC"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "Web Security Group"
  }
}

# resource "aws_instance" "foo" {
#   ami           = "ami-06e46074ae430fba6" # us-west-1 - Amazon Linux 2023 AMI
#   instance_type = "t2.micro"
# 	subnet_id =  aws_subnet.lab_subnet_public1.id
# 	key_name = "vockey"

#   vpc_security_group_ids = [aws_security_group.allow_ssh.id]


#   tags = {
#     "Name" = "foo"
#   }
# }

# resource "aws_eip" "foo_eip" {
#   vpc = true
# }

# resource "aws_eip_association" "foo_eip_association" {
#   instance_id   = aws_instance.foo.id
#   allocation_id = aws_eip.foo_eip.id
# }

# resource "aws_instance" "bar" {
#   ami           = "ami-06e46074ae430fba6" # us-west-1 - Amazon Linux 2023 AMI
#   instance_type = "t2.micro"
# 	subnet_id =  aws_subnet.lab_subnet_private1.id
# 	key_name = "vockey"

#   vpc_security_group_ids = [aws_security_group.allow_ssh.id]


#   tags = {
#     "Name" = "bar"
#   }
# }

resource "aws_security_group" "allow_ssh" {
  name        = "SSH Security Group"
  description = "Enable SSH access"
  vpc_id      = aws_vpc.lab_vpc.id

  ingress {
    description      = "SSH from VPC"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "SSH Security Group"
  }
}